using UnityEngine;

public class MicInput : MonoBehaviour
{
    AudioClip microphoneInput;
    AudioSource audioSource;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        microphoneInput = Microphone.Start(null, true, 10, 44100); // Adjust the duration and frequency as needed
        audioSource.clip = microphoneInput;
        audioSource.loop = true;
        while (!(Microphone.GetPosition(null) > 0)) { } // Wait until the microphone starts recording
        audioSource.Play();
    }

    void Update()
    {
        // Optional: You can process the audio input in real-time here
    }

    void OnDisable()
    {
        Microphone.End(null);
    }
}