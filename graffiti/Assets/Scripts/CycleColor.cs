using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CycleColor : MonoBehaviour
{
    public MeshRenderer rend;
    public float colorChangeSpeed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
                     // Change color over time based on colorChangeSpeed        
            float hue = Mathf.PingPong(Time.time * colorChangeSpeed, 1f); 
            Color currentColor = Color.HSVToRGB(hue, 1f, 1f);       
            // Apply the color to the ribbon material     
           
            //update all child materials;

                rend.sharedMaterial.color = currentColor;
            
    }
}
